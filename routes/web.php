<?php

use Illuminate\Support\Facades\Route;

Route::get('cek', function () {
    $userId = auth()->user()->id;
    $c = \Cart::session($userId)->getContent();
    $k = \Cart::session($userId)->getTotal();
    $b = \Cart::session($userId)->getSubTotal();
    return $c;
});

Auth::routes();

// landing

Route::get('/', [App\Http\Controllers\Customer\DashboardController::class, 'index'])->name('customer.dashboard');
Route::get('/autentikasi', [App\Http\Controllers\HomeController::class, 'auth'])->name('customer.auth');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/produk', [App\Http\Controllers\Customer\ProdukController::class, 'index']);
Route::get('/detailproduk/{id}', [App\Http\Controllers\Customer\DashboardController::class, 'detailproduk'])->name('customer.detailproduk');
Route::get('get_provinsi', [App\Http\Controllers\AddressController::class, 'getProvinsi']);
Route::get('get_kota', [App\Http\Controllers\AddressController::class, 'getCities']);

Route::get('/about', function () {
    return view('konsumen.about');
})->name('customer.about');


Route::middleware(['auth'])->group(
    function () {
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        // Profil customer
        Route::get('/akunsaya', [App\Http\Controllers\Customer\DashboardController::class, 'myAccount'])->name('customer.account');
        Route::get('/detail-tr/{id}', [App\Http\Controllers\Customer\DashboardController::class, 'detailTransaksi']);

        // Keranjang
        Route::get('keranjang', [App\Http\Controllers\Customer\PembelianController::class, 'keranjang'])->name('customer.keranjang');
        Route::get('addkeranjang/{id}', [App\Http\Controllers\Customer\PembelianController::class, 'tambahkeranjang'])->name('customer.tambahkeranjang');
        Route::post('updatekeranjang', [App\Http\Controllers\Customer\PembelianController::class, 'updatekeranjang'])->name('customer.updatekeranjang');
        Route::get('deletekeranjang/{id}', [App\Http\Controllers\Customer\PembelianController::class, 'deletekeranjang'])->name('customer.deletekeranjang');

        // Checkout
        Route::get('checkout', [App\Http\Controllers\Customer\PembelianController::class, 'Checkout'])->name('customer.checkout');
        Route::post('prosescheckout', [App\Http\Controllers\Customer\PembelianController::class, 'ProsesCheckout'])->name('customer.prosescheckout');

        Route::post('upload_bukti_bayar', [App\Http\Controllers\Customer\DashboardController::class, 'uploadBuktiTransfer']);


        Route::get('chat', [App\Http\Controllers\Customer\ChatController::class, 'index']);
        Route::get('load-message', [App\Http\Controllers\Customer\ChatController::class, 'loadMessage']);
        Route::post('send-message', [App\Http\Controllers\Customer\ChatController::class, 'sendMessage']);
        Route::get('unread-message', [App\Http\Controllers\Customer\ChatController::class, 'cekUnReadMessage']);

        Route::post('update_barang_sampai', [App\Http\Controllers\Customer\DashboardController::class, 'updateStatusSampai']);
        Route::post('update_alamat_user', [App\Http\Controllers\Customer\DashboardController::class, 'updateAlamat']);
        Route::post('update_profil_user', [App\Http\Controllers\Customer\DashboardController::class, 'updateProfil']);

        // user admin
        Route::prefix('/admin')->group(function () {

            // dashboard
            Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');

            // pembelian
            Route::get('/pembelian', [App\Http\Controllers\Admin\PembelianController::class, 'index'])->name('admin.pembelian');
            Route::get('/pembelian/{id}/edit', [App\Http\Controllers\Admin\PembelianController::class, 'edit']);
            Route::post('/pembelian/hapus', [App\Http\Controllers\Admin\PembelianController::class, 'destroy'])->name('admin.pembelian-hapus');
            Route::post('/pembelian/verif', [App\Http\Controllers\Admin\PembelianController::class, 'verif'])->name('admin.pembelian-verif');

            // pengiriman
            Route::get('/pengiriman', [App\Http\Controllers\Admin\PengirimanController::class, 'index'])->name('admin.pengiriman');
            Route::get('/pengiriman/{id}/edit', [App\Http\Controllers\Admin\PengirimanController::class, 'edit']);
            Route::post('/pengiriman/kirim', [App\Http\Controllers\Admin\PengirimanController::class, 'kirim'])->name('admin.pengiriman-kirim');

            // barang
            Route::get('/barang', [App\Http\Controllers\Admin\BarangController::class, 'index'])->name('admin.barang');
            Route::post('/barang/tambah', [App\Http\Controllers\Admin\BarangController::class, 'store'])->name('admin.barang-tambah');
            Route::get('/barang/{id}/edit', [App\Http\Controllers\Admin\BarangController::class, 'edit']);
            Route::post('/barang/hapus', [App\Http\Controllers\Admin\BarangController::class, 'destroy'])->name('admin.barang-hapus');
            Route::post('/barang/tampilkan', [App\Http\Controllers\Admin\BarangController::class, 'tampilkan'])->name('admin.barang-tampilkan');

            // stok
            Route::get('/stok', [App\Http\Controllers\Admin\BarangController::class, 'indexStok'])->name('admin.stok');

            // user
            Route::get('/user', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.user');
            Route::get('/user-admin', [App\Http\Controllers\Admin\UserController::class, 'indexAdmin'])->name('admin.admin');
            Route::post('/user/tambah', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('admin.user-tambah');
            Route::get('/user/{id}/edit', [App\Http\Controllers\Admin\UserController::class, 'edit']);
            Route::post('/user/hapus', [App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('admin.user-hapus');
            Route::patch('/user/update', [App\Http\Controllers\Admin\UserController::class, 'update'])->name('admin.user-update');

            // alamat user
            Route::get('/alamat-user', [App\Http\Controllers\Admin\UserController::class, 'indexAlamat'])->name('admin.alamat-user');

            // bank
            Route::get('/bank', [App\Http\Controllers\Admin\BankController::class, 'index'])->name('admin.bank');
            Route::post('/bank/tambah', [App\Http\Controllers\Admin\BankController::class, 'store'])->name('admin.bank-tambah');
            Route::get('/bank/{id}/edit', [App\Http\Controllers\Admin\BankController::class, 'edit']);
            Route::post('/bank/hapus', [App\Http\Controllers\Admin\BankController::class, 'destroy'])->name('admin.bank-hapus');

            // satuan
            Route::get('/satuan', [App\Http\Controllers\Admin\SatuanController::class, 'index'])->name('admin.satuan');
            Route::post('/satuan/tambah', [App\Http\Controllers\Admin\SatuanController::class, 'store'])->name('admin.satuan-tambah');
            Route::get('/satuan/{id}/edit', [App\Http\Controllers\Admin\SatuanController::class, 'edit']);
            Route::post('/satuan/hapus', [App\Http\Controllers\Admin\SatuanController::class, 'destroy'])->name('admin.satuan-hapus');

            // chat
            Route::get('/chat', [App\Http\Controllers\Admin\ChatController::class, 'index'])->name('admin.chat');
            Route::post('/chat/tambah', [App\Http\Controllers\Admin\ChatController::class, 'store'])->name('admin.chat-tambah');
            Route::get('/chat/{id}/edit', [App\Http\Controllers\Admin\ChatController::class, 'edit']);

            Route::get('/load-message/{id}', [App\Http\Controllers\Admin\ChatController::class, 'loadMessage'])->name('admin.load-message');
            Route::post('/send-message', [App\Http\Controllers\Admin\ChatController::class, 'sendMessage']);
            Route::get('/unread-message', [App\Http\Controllers\Admin\ChatController::class, 'cekUnReadMessage']);

            // laporan
            Route::get('/laporan', [App\Http\Controllers\Admin\LaporanController::class, 'index'])->name('admin.laporan-pembelian');

            // about
            Route::get('/about', function () {
                return view('about');
            })->name('admin.about');
        });
        // user owner
        Route::prefix('/owner')->group(function () {

            // dashboard
            Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('owner.dashboard');

            // laporan
            Route::get('/laporan', [App\Http\Controllers\Admin\LaporanController::class, 'index'])->name('owner.laporan-pembelian');

            // about
            Route::get('/about', function () {
                return view('about');
            })->name('owner.about');
        });
    }
);
