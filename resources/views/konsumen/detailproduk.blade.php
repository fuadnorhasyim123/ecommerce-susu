@extends('konsumen.layouts.master')
@section('content')
<!-- ::::::  Start  Breadcrumb Section  ::::::  -->
<div class="page-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="page-breadcrumb__menu">
                    <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                    <li class="page-breadcrumb__nav active">Detail Produk {{ $data->barang_nama }}</li>
                </ul>
            </div>
        </div>
    </div>
</div> <!-- ::::::  End  Breadcrumb Section  ::::::  -->

<!-- ::::::  Start  Main Container Section  ::::::  -->
<main id="main-container" class="main-container">
    <div class="container">
        <div class="row">
            <!-- Start Product Details Gallery -->
            <div class="col-12">
                <div class="product-details">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="product-gallery-box m-b-60">
                                <div class="product-image--large overflow-hidden">
                                    <img class="img-fluid" id="img-zoom" src="{{ asset('images/'.$data->barang_gambar) }}" data-zoom-image="{{ asset('images/'.$data->barang_gambar) }}" alt="">
                                </div>
                                {{-- <div class="pos-relative m-t-30">
                                        <div id="gallery-zoom"
                                            class="product-image--thumb product-image--thumb-horizontal overflow-hidden swiper-outside-arrow-hover m-lr-30">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <a class="zoom-active"
                                                        data-image="assets/img/product/gallery/gallery-large/product-gallery-large-1.jpg"
                                                        data-zoom-image="assets/img/product/gallery/gallery-large/product-gallery-large-1.jpg">
                                                        <img class="img-fluid"
                                                            src="assets/img/product/gallery/gallery-thumb/product-gallery-thumb-1.jpg"
                                                            alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a data-image="assets/img/product/gallery/gallery-large/product-gallery-large-2.jpg"
                                                        data-zoom-image="assets/img/product/gallery/gallery-large/product-gallery-large-2.jpg">
                                                        <img class="img-fluid"
                                                            src="assets/img/product/gallery/gallery-thumb/product-gallery-thumb-2.jpg"
                                                            alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a data-image="assets/img/product/gallery/gallery-large/product-gallery-large-3.jpg"
                                                        data-zoom-image="assets/img/product/gallery/gallery-large/product-gallery-large-3.jpg">
                                                        <img class="img-fluid"
                                                            src="assets/img/product/gallery/gallery-thumb/product-gallery-thumb-3.jpg"
                                                            alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a data-image="assets/img/product/gallery/gallery-large/product-gallery-large-4.jpg"
                                                        data-zoom-image="assets/img/product/gallery/gallery-large/product-gallery-large-4.jpg">
                                                        <img class="img-fluid"
                                                            src="assets/img/product/gallery/gallery-thumb/product-gallery-thumb-4.jpg"
                                                            alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a data-image="assets/img/product/gallery/gallery-large/product-gallery-large-5.jpg"
                                                        data-zoom-image="assets/img/product/gallery/gallery-large/product-gallery-large-5.jpg">
                                                        <img class="img-fluid"
                                                            src="assets/img/product/gallery/gallery-thumb/product-gallery-thumb-5.jpg"
                                                            alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-buttons">
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next gallery__nav gallery__nav--next"><i
                                                    class="fal fa-chevron-right"></i></div>
                                            <div class="swiper-button-prev gallery__nav gallery__nav--prev"><i
                                                    class="fal fa-chevron-left"></i></div>
                                        </div>
                                    </div> --}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-details-box">
                                <h5 class="section-content__title">{{ $data->barang_nama }}</h5>
                                <div class="product__price">
                                    <span class="product__price-reg">Rp.
                                        {{ number_format($data->barang_harga, 0) }}</span>
                                </div>
                                <div class="product__desc m-t-25 m-b-30">
                                    <p>Kode Barang : <b>{{ $data->barang_kode }}</b></p>
                                    <p>Satuan Barang : <b>{{ $data->barang_satuan }}</b></p>
                                    <p>Stok Tersisa : <b>{{ $data->barang_stok }}</b></p>
                                    <p>Tanggal Datang : <b>{{ \Carbon\Carbon::parse($data->created_at)->format('d-m-Y') }}</b></p>
                                    <p>Tanggal Kadaluarsa : <b>{{ \Carbon\Carbon::parse($data->barang_tglexpired)->format('d-m-Y') }}</b></p>
                                </div>
                                <div class="product-var p-t-30">
                                    <div class="product-quantity product-var__item">
                                        <span class="product-var__text">Jumlah Barang</span>
                                        <form method="GET" action="{{ url('addkeranjang/'.$data->barang_id) }}">
                                            <div class="product-quantity-box">
                                                <div class="quantity">
                                                    <input type="number" name="qty" min="1" max="{{ $data->barang_stok }}" step="1" value="1">
                                                </div>
                                                <button type="submit" class="btn btn--box btn--small btn--blue btn--uppercase btn--weight m-l-20">Tambah Keranjang</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Product Details Gallery -->
        </div>
    </div>
</main>
@endsection