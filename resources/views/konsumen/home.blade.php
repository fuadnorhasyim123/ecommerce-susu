@extends('konsumen.layouts.master')
@section('content')

<main id="main-container" class="main-container">

    <div class="product product--1 swiper-outside-arrow-hover">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-content section-content--border d-flex align-items-center justify-content-between">
                        <h5 class="section-content__title">Daftar Produk </h5>
                        <a href="{{ url('produk') }}">Semua Produk<i class="icon-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="swiper-outside-arrow-fix pos-relative">
                        <div class="product-default-slider-5grid overflow-hidden  m-t-50">
                            <div class="swiper-wrapper">
                                <!-- Start Single Default Product -->
                                @foreach ($produk as $item)
                                <div class="product__box product__box--default product__box--border-hover swiper-slide text-center">
                                    <div class="product__img-box">
                                        <a href="{{route('customer.detailproduk',$item->barang_id)}}" class="product__img--link">
                                            <img class="product__img" src="{{asset('images/'.$item->barang_gambar)}}" alt="">
                                        </a>
                                        <a href="{{ route('customer.tambahkeranjang', $item->barang_id) }}" class="btn btn--box btn--small btn--gray btn--uppercase btn--weight btn--hover-zoom product__upper-btn">Keranjang</a>
                                    </div>
                                    <div class="product__price m-t-10">
                                        <span class="product__price-reg">{{number_format($item->barang_harga,0)}}</span>
                                    </div>
                                    Sisa : {{$item->barang_stok}}
                                    <a href="{{route('customer.detailproduk',$item->barang_id)}}" class="product__link product__link--underline product__link--weight-light m-t-15">
                                        <h5>{{$item->barang_nama}}</h5>
                                        {{$item->barang_satuan}}
                                    </a>
                                </div> <!-- End Single Default Product -->
                                @endforeach
                            </div>
                            <div class="swiper-buttons">
                                <!-- Add Arrows -->
                                <div class="swiper-button-next default__nav default__nav--next"><i class="fal fa-chevron-right"></i></div>
                                <div class="swiper-button-prev default__nav default__nav--prev"><i class="fal fa-chevron-left"></i></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</main>

@endsection