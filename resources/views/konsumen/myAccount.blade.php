@extends('konsumen.layouts.master')
@section('content')
<!-- ::::::  Start  Breadcrumb Section  ::::::  -->
<!-- <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="page-breadcrumb__menu">
                        <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                        <li class="page-breadcrumb__nav active">Akun saya</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>  -->
<!-- ::::::  End  Breadcrumb Section  ::::::  -->

<!-- ::::::  Start  Main Container Section  ::::::  -->
<main id="main-container" class="main-container">
    <div class="row p-5">
        <div class="col-12">
            <!-- :::::::::: Start My Account Section :::::::::: -->
            <div class="my-account-area">
                <div class="row">
                    <div class="col-xl-4 col-md-4">
                        <div class="my-account-menu">
                            <ul class="nav account-menu-list flex-column nav-pills" id="pills-tab" role="tablist">
                                <li>
                                    <a class="active link--icon-left" id="pills-dashboard-tab" data-toggle="pill" href="#pills-dashboard" role="tab" aria-controls="pills-dashboard" aria-selected="true"><i class="fas fa-tachometer-alt"></i> Utama</a>
                                </li>
                                <li>
                                    <a id="pills-order-tab" class="link--icon-left" data-toggle="pill" href="#pills-order" role="tab" aria-controls="pills-order" aria-selected="false"><i class="fas fa-shopping-cart"></i> Transaksi <span style="float: right; margin-top: 15px;" class="badge badge-primary badge-pill">{{ $data['belumBayar'] }}</span></a>
                                </li>
                                <li>
                                    <a id="pills-order-tab" class="link--icon-left" data-toggle="pill" href="#pills-pengiriman" role="tab" aria-controls="pills-pengiriman" aria-selected="false"><i class="fas fa-truck"></i> Pengiriman</a>
                                </li>
                                <li>
                                    <a id="pills-address-tab" class="link--icon-left" data-toggle="pill" href="#pills-address" role="tab" aria-controls="pills-address" aria-selected="false"><i class="fas fa-lock"></i> Ubah Kata Sandi</a>
                                </li>
                                <li>
                                    <a id="pills-account-tab" class="link--icon-left" data-toggle="pill" href="#pills-account" role="tab" aria-controls="pills-account" aria-selected="false"><i class="fas fa-user"></i>
                                        Detail Akun</a>
                                </li>
                                <li>
                                    <a class="link--icon-left" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                                                document.getElementById('logout-form').submit();">
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form><i class="fas fa-sign-out-alt"></i> Keluar Aplikasi
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-8 col-md-8">
                        @if (session('status') == 'Password Baru dengan Konfirmasi Password Tidak Sama' || session('status') == 'Password anda tidak cocok dengan password anda login')
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                        @elseif(session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <div class="tab-content my-account-tab" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-dashboard" role="tabpanel" aria-labelledby="pills-dashboard-tab">
                                <div class="my-account-dashboard account-wrapper">
                                    <h4 class="account-title">Utama</h4>
                                    <div class="welcome-dashboard m-t-30">
                                        <p>Hallo, <strong>{{ $data['akun']->name }}</strong> </p>
                                    </div>
                                    <p class="m-t-25">Pada akun, kamu dapat dengan mudah mengecek
                                        &amp; Pesanan kamu yang terbaru
                                        , dan mengubah password dan detail akun.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-order" role="tabpanel" aria-labelledby="pills-order-tab">
                                <div class="my-account-order account-wrapper">
                                    <h4 class="account-title">Pesanan</h4>
                                    <div class="account-table text-center m-t-30 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="no">No</th>
                                                    <th class="name">Kode Pesanan</th>
                                                    <th class="date">Tanggal</th>
                                                    <th class="status">Status</th>
                                                    <th class="total">Total</th>
                                                    <th class="action">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no = 1;
                                                @endphp
                                                @foreach ($data['pesanan'] as $item)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ 'SUSU-' . $item->id . '-' . $item->id_konsumen }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>
                                                        @if ($item->bukti_transfer == null && $item->is_verified == 0)
                                                        <span class="text-primary">Menunggu Pembayaran</span>
                                                        @elseif($item->bukti_transfer != null && $item->is_verified == 0)
                                                        <span class="text-info">Menunggu Verifikasi</span>
                                                        @elseif($item->bukti_transfer != null && $item->is_verified == 1)
                                                        <span class="text-success">Terverifikasi</span>
                                                        @elseif($item->bukti_transfer != null && $item->is_verified == 2)
                                                        <span class="text-danger">Ditolak</span>
                                                        @endif
                                                    </td>
                                                    <td>Rp. {{ number_format($item->total_tr, 2, ',', '.') }}</td>
                                                    <td><a href="{{ url('detail-tr/' . $item->id) }}">Lihat</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pills-pengiriman" role="tabpanel" aria-labelledby="pills-pengiriman-tab">
                                <div class="my-account-pengiriman account-wrapper">
                                    <h4 class="account-title">Pengiriman Barang</h4>
                                    <div class="account-table text-center m-t-30 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="no">No</th>
                                                    <th class="name">Kode Pesanan</th>
                                                    <th class="date">Tanggal Pembelian</th>
                                                    <th class="date">Tanggal Pengiriman</th>
                                                    <th class="status">Status</th>
                                                    <th class="action"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no = 1;
                                                @endphp
                                                @foreach ($data['pengiriman'] as $item)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ 'SUSU-' . $item->id . '-' . $item->id_konsumen }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>{{ $item->tanggal_pengiriman }}</td>
                                                    <td>
                                                        @if ($item->status_dikirim == 0)
                                                        <span class="text-info">Belum Dikirim</span>
                                                        @elseif($item->status_dikirim == 1 && $item->status_sampai == 0)
                                                        <span class="text-primary">Dikirim</span>
                                                        @elseif($item->status_dikirim == 1 && $item->status_sampai == 1)
                                                        <span class="text-success">Sampai</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($item->status_dikirim == 1 && $item->status_sampai == 0)
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{ $item->id }}">
                                                            Konfirmasi
                                                        </button>
                                                        @elseif($item->status_sampai == 1)
                                                        <span class="text-info">Barang Telah Sampai</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Konfirmasi Barang Sampai</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                Pastikan barang pada transaksi
                                                                {{ 'SUSU-' . $item->id . '-' . $item->id_konsumen }}
                                                                telah sampai.
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form method="POST" action="{{ url('update_barang_sampai') }}">
                                                                    @csrf
                                                                    <input type="hidden" name="id_pembelian" value="{{ $item->id }}" />
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak, Barang Belum
                                                                        Sampai</button>
                                                                    <button class="btn btn-primary" type="submit">Ya, Barang Telah
                                                                        Sampai</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                                <div class="my-account-payment account-wrapper">
                                    <h4 class="account-title">Alamat Anda</h4>
                                    <p class="m-t-30">{{ Auth::user()->alamat }}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-address" role="tabpanel" aria-labelledby="pills-address-tab">
                                <div class="my-account-address account-wrapper">
                                    <h4 class="account-title">Ubah Kata Sandi</h4>
                                    <div class="account-address m-t-30">
                                        <form id="editAlamat" method="POST" action="{{ url('update_alamat_user') }}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <input type="password" name="current_password" placeholder="Kata Sandi Saat Ini">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-box__single-group">
                                                        <input type="password" name="password_baru" placeholder="Kata Sandi Baru">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-box__single-group">
                                                        <input type="password" name="password_konfirmasi" placeholder="Konfirmasi Kata Sandi">
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary p-3 mt-3">Ubah</button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pills-account" role="tabpanel" aria-labelledby="pills-account-tab">
                                <div class="my-account-details account-wrapper">
                                    <h4 class="account-title">Detail Akun</h4>

                                    <div class="account-details">
                                        <form action="{{ url('update_profil_user') }}" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Nama Lengkap</label>
                                                        <input type="text" name="name" placeholder="Nama Lengkap" value="{{ Auth::user()->name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Email</label>
                                                        <input type="email" name="email" placeholder="Email address" value="{{ Auth::user()->email }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Telepon</label>
                                                        <input type="text" name="telepon" placeholder="Telepon" value="{{ Auth::user()->telepon }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Provinsi</label>
                                                        <select onChange="pilihProvinsi()" name="provinsi_id" id='select_provinsi'>
                                                            @foreach ($data['provinsi'] as $item)
                                                            <option value="{{ $item->id }}">{{ $item->text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Kota</label>
                                                        <select onChange="pilihProvinsi()" name="kota_id" id='select_kota'>
                                                            @foreach ($data['kota'] as $item)
                                                            <option value="{{ $item->id }}">{{ $item->text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Kode POS</label>
                                                        <input type="text" name="kode_pos" placeholder="Kode POS" value="{{ Auth::user()->kode_pos }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <label>Alamat</label>
                                                        <textarea class="form-control" name="alamat">{{ Auth::user()->alamat }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-box__single-group">
                                                        <button type="submit" class="btn btn--box btn--small btn--uppercase btn--blue">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- :::::::::: End My Account Section :::::::::: -->
        </div>
    </div>
</main> <!-- ::::::  End  Main Container Section  ::::::  -->
@endsection

@push('scripts')
<script>
    $("#btnEditAlamat").on("click", function() {
        $("#editAlamat").removeClass("hidden");
    });

    $(document).ready(function() {
        $('#select_provinsi').select2();
        $('#select_provinsi').val('{{ Auth::user()->provinsi_id }}').trigger('change.select2');
        $('#select_kota').select2();
        $('#select_kota').val('{{ Auth::user()->kota_id }}').trigger('change.select2');
    });

    function pilihProvinsi() {
        var provinsi = $("#select_provinsi").val();
        console.log(provinsi);
        loadKota(provinsi);
    }

    function loadKota(provinsiId) {
        $("#select_kota").select2({
            ajax: {
                url: "{{ url('get_kota') }}" + "?provinsi=" + provinsiId,
                type: "GET",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        search: params.term // search term
                    };
                },
                processResults: function(response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
    }
</script>
@endpush