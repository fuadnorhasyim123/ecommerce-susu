<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <title>Ecommerce Susu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- ::::::::::::::Favicon icon::::::::::::::-->
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

    <!-- ::::::::::::::All CSS Files here :::::::::::::: -->

    <!-- Vendor CSS Files -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/vendor/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/vendor/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/vendor/plaza-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/vendor/bootstrap.min.css') }}">

    <!-- Plugin CSS Files -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/plugin/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/plugin/material-scrolltop.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/plugin/price_range_style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/plugin/in-number.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/plugin/venobox.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <!-- <link rel="stylesheet" href="assets/css/vendor/vendor.min.css"/>
    <link rel="stylesheet" href="assets/css/plugin/plugins.min.css"/>
    <link rel="stylesheet" href="assets/css/main.min.css"> -->

    <!-- Main Style CSS File -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/main.css') }}">
    <style>
        .img-bukti-transfer {
            width: 100%;
        }

        .chat-container {
            width: 100%;
            height: 70vh;
            padding: 2px;
            overflow-y: scroll;
            overflow-x: hidden;
        }

        .bottom-position {
            width: 100%;
            position: absolute;
            bottom: 0px;
        }

        .hidden {
            display: none;
        }
    </style>
</head>

<body>
    <!-- ::::::  Start  Header Section  ::::::  -->
    <header>
        <!-- ::::::  Start Large Header Section  ::::::  -->
        <div class="header header--1">
            @include('sweetalert::alert')
            @php
            $userId = Auth::user();
            if ($userId !== null) {
            $cart = \Cart::session(Auth::user()->id)->getContent();
            } else {
            $cart = \Cart::getContent();
            }
            @endphp
            <!-- Start Header Top area -->
            <div class="header__top header__top--style-1">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="header__top-content">
                                <div class="header__top-content--left">
                                    <div class="contact_cms">
                                        <span class="cms1">Telepon/Whatsapp: </span>
                                        <span class="cms2">082335876013</span>
                                    </div>
                                </div>
                                <div class="header__top-content--right">
                                    <div class="user-info user-set-role">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- Start Header Top area -->
            <!-- Start Header Middle area -->
            <div class="header__middle header__top--style-1 p-tb-30">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="header__logo">
                                <a href="index.html" class="header__logo-link">
                                    <img src="assets/img/logo/logo-color.jpg" alt="" class="header__logo-img">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row align-items-center">
                                <div class="col-lg-8">
                                </div>
                                <div class="col-lg-4">
                                    <div class="header__wishlist-box">
                                        <!-- Start Header Wishlist Box -->
                                        <div class="header__wishlist pos-relative">
                                            <a href="{{ url('chat') }}" class="header__wishlist-link mr-4">
                                                <i class="fa fa-comments"></i>
                                                <span id="loadMessageUnread" class="wishlist-item-count pos-absolute hidden"></span>
                                            </a>
                                        </div>
                                        <div class="header__wishlist pos-relative">
                                            @if (Auth::check())
                                            <?php
                                            $belumBayar = \DB::table('pembelian as tr')
                                                ->join('bukti_transfer as tf', 'tf.id_pembelian', '=', 'tr.id')
                                                ->join('pembayaran as byr', 'byr.id_pembelian', '=', 'tr.id')
                                                ->join('bank as b', 'b.id', '=', 'byr.bank_id')
                                                ->where('tr.id_konsumen', \Auth::user()->id)
                                                ->whereNull('tf.bukti_transfer')
                                                ->where('tf.is_verified', 0)
                                                ->count();
                                            ?>
                                            <a href="{{ route('customer.account') }}" class="header__wishlist-link">
                                                <span></span>
                                                <i class="icon-users"></i>
                                                @if($belumBayar > 0)
                                                <span class="wishlist-item-count pos-absolute">{{ $belumBayar }}</span>
                                                @endif
                                            </a>
                                            @else
                                            <a href="{{ route('customer.account') }}" class="header__wishlist-link">
                                                <span></span>
                                                <i class="icon-users"></i>
                                            </a>
                                            @endif
                                        </div> <!-- End Header Wishlist Box -->

                                        <!-- Start Header Add Cart Box -->
                                        <div class="header-add-cart pos-relative m-l-40">
                                            <a href="{{ route('customer.keranjang') }}">
                                                <i class="icon-shopping-cart"></i>
                                                <span class="wishlist-item-count pos-absolute">{{ count($cart) }}</span>
                                            </a>
                                        </div> <!-- End Header Add Cart Box -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Header Middle area -->

            <!-- Start Header Menu Area -->
            <div class="header-menu">
                <div class="container">
                    <div class="row col-12">
                        <nav>
                            <ul class="header__nav">
                                <!--Start Single Nav link-->
                                <li class="header__nav-item pos-relative">
                                    <a href="{{ url('/') }}" class="header__nav-link">Beranda</a>
                                </li> <!-- End Single Nav link-->

                                <!--Start Single Nav link-->
                                <li class="header__nav-item pos-relative">
                                    <a href="{{ url('produk') }}" class="header__nav-link">Produk</a>
                                </li> <!-- End Single Nav link-->
                                <!--Start Single Nav link-->
                                <li class="header__nav-item pos-relative">
                                    <a href="{{ route('customer.about') }}" class="header__nav-link">Tentang Toko</a>
                                </li> <!-- End Single Nav link-->
                            </ul>
                        </nav>
                    </div>
                </div>
            </div> <!-- End Header Menu Area -->

            <!-- Start Header Tag Area -->
            <div class="header__bottom header__bottom--style-1">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="header__tag">
                                <span class="header__tag-title"></span>
                                <ul class="header__tag-nav">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Header Tag Area -->
        </div> <!-- ::::::  End Large Header Section  ::::::  -->

        <!-- ::::::  Start Mobile Header Section  ::::::  -->
        <div class="header__mobile mobile-header--1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Start Header Mobile Top area -->
                        <div class="header__mobile-top">
                            <div class="mobile-header__logo">
                                <a href="index.html" class="mobile-header__logo-link">
                                    <img src="assets/img/logo/logo-color.jpg" alt="" class="mobile-header__logo-img">
                                </a>
                            </div>
                            <div class="header__wishlist-box">
                                <!-- Start Header Wishlist Box -->
                                <div class="header__wishlist pos-relative">
                                    <a href="wishlist.html" class="header__wishlist-link">
                                        <i class="icon-heart"></i>
                                        <span class="wishlist-item-count pos-absolute">3</span>
                                    </a>
                                </div> <!-- End Header Wishlist Box -->

                                <!-- Start Header Add Cart Box -->
                                <div class="header-add-cart pos-relative m-l-20">
                                    <a href="#offcanvas-add-cart__box" class="header__wishlist-link offcanvas--open-checkout offcanvas-toggle">
                                        <i class="icon-shopping-cart"></i>
                                        <span class="wishlist-item-count pos-absolute">3</span>
                                    </a>
                                </div> <!-- End Header Add Cart Box -->

                                <a href="#offcanvas-mobile-menu" class="offcanvas-toggle m-l-20"><i class="icon-menu"></i></a>

                            </div>
                        </div> <!-- End Header Mobile Top area -->

                        <!-- Start Header Mobile Middle area -->
                        <div class="header__mobile-middle header__top--style-1 p-tb-10">
                            <form class="header__search-form" action="#">
                                <div class="header__search-input header__search-input--mobile">
                                    <input type="search" placeholder="Enter your search">
                                    <button class="btn btn--submit btn--blue btn--uppercase btn--weight" type="submit"><i class="fal fa-search"></i></button>
                                </div>
                            </form>
                        </div> <!-- End Header Mobile Middle area -->

                    </div>
                </div>
            </div>
        </div> <!-- ::::::  Start Mobile Header Section  ::::::  -->

        <!-- ::::::  Start Mobile-offcanvas Menu Section  ::::::  -->
        <div id="offcanvas-mobile-menu" class="offcanvas offcanvas-mobile-menu">
            <button class="offcanvas__close offcanvas-close">&times;</button>
            <div class="offcanvas-inner">
                <!-- <div class="offcanvas-userpanel m-b-30">
                    <ul>
                        <li class="offcanvas-userpanel__role">
                            <a href="#">Setting</a>
                            <ul class="user-sub-menu">
                                <li><a href="my-account.html">Akun Saya</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="login.html">Sign in</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> -->

                <div class="offcanvas-menu m-b-30">
                    <h4>Menu</h4>
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ route('customer.about') }}">About</a></li>
                    </ul>
                </div>
                <div class="offcanvas-buttons m-b-30">
                    <a href="my-account.html" class="user"><i class="icon-user"></i></a>
                    <a href="{{ route('customer.keranjang') }}"><i class="icon-shopping-cart"></i></a>
                </div>
            </div>
        </div> <!-- ::::::  End Mobile-offcanvas Menu Section  ::::::  -->


        <div class="offcanvas-overlay"></div>
    </header> <!-- ::::::  End  Header Section  ::::::  -->

    @yield('content')
    <div class="my-5"></div>
    <!-- ::::::  Start  Footer Section  ::::::  -->
    <footer class="footer">
        <div class="footer__top gray-bg p-tb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="index.html" class="footer__logo-link">
                                    <img src="assets/img/logo/logo-color.jpg" alt="" class="footer__logo-img">
                                </a>
                            </div>
                            <div class="footer__text">
                                <p>Menyediakan susu terbaik dengan kualitas premium</p>
                            </div>
                            <ul class="footer__address">
                                <li class="footer__address-item"><span>Alamat:</span> Dusun Samar, Desa Mulyosari, Kec. Pagerwojo, Kab. Tulungagung</li>
                                <li class="footer__address-item"><span>Hubungi: </span> 082335876013 </li>
                                <li class="footer__address-item"><span>Email: </span> tokosusu@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="footer__menu">
                                    <h4 class="footer__nav-title">Ikuti Kita</h4>
                                    <ul class="footer__social-nav">
                                        <li class="footer__social-list"><a href="#" class="footer__social-link"><i class="fab fa-facebook-f"></i></a></li>
                                        <!-- <li class="footer__social-list"><a href="#" class="footer__social-link"><i class="fab fa-twitter"></i></a></li> -->
                                        <li class="footer__social-list"><a href="#" class="footer__social-link"><i class="fab fa-youtube"></i></a></li>
                                        <!-- <li class="footer__social-list"><a href="#" class="footer__social-link"><i class="fab fa-google-plus-g"></i></a></li> -->
                                        <li class="footer__social-list"><a href="#" class="footer__social-link"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-12">
                        <div class="footer__copyright-text">
                            <p>Copyright @2022. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="footer__payment">
                            <a href="https://hasthemes.com/" class="footer__payment-link">
                                <img src="assets/img/company-logo/payment.png" alt="" class="footer__payment-img">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer> <!-- ::::::  End  Footer Section  ::::::  -->

    <!-- material-scrolltop button -->
    <button class="material-scrolltop" type="button"></button>

    <!-- Start Modal Add cart -->
    <!-- End Modal Add cart -->


    <!-- ::::::::::::::All Javascripts Files here ::::::::::::::-->

    <!-- Vendor JS Files -->
    <script src="{{ asset('landing/assets/js/vendor/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('landing/assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>
    <script src="{{ asset('landing/assets/js/vendor/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('landing/assets/js/vendor/bootstrap.bundle.js') }}"></script>

    <!-- Plugins JS Files -->
    <script src="{{ asset('landing/assets/js/plugin/swiper.min.js') }}"></script>
    <script src="{{ asset('landing/assets/js/plugin/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('landing/assets/js/plugin/material-scrolltop.js') }}"></script>
    <script src="{{ asset('landing/assets/js/plugin/price_range_script.js') }}"></script>
    <script src="{{ asset('landing/assets/js/plugin/in-number.js') }}"></script>
    <script src="{{ asset('landing/assets/js/plugin/jquery.elevateZoom-3.0.8.min.js') }}"></script>
    <script src="{{ asset('landing/assets/js/plugin/venobox.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <!-- <script src="assets/js/vendor/vendor.min.js"></script>
    <script src="assets/js/plugin/plugins.min.js"></script> -->

    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="{{ asset('landing/assets/js/main.js') }}"></script>

    <script>
        $(document).ready(function() {
            bsCustomFileInput.init()
        })
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        setInterval(() => cekUnreadMessage(), 2000);

        function cekUnreadMessage() {
            $.ajax({
                url: '{{ url("unread-message") }}',
                type: 'GET',
                success: function(result) {
                    $("#loadMessageUnread").html(result.data);
                    if (parseInt(result.data) > 0) {
                        $("#loadMessageUnread").removeClass("hidden");
                    }
                }
            })
        }
    </script>

    @stack('scripts')
</body>

</html>