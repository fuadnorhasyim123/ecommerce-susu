@extends('konsumen.layouts.master')
@section('content')
    <!-- ::::::  Start  Breadcrumb Section  ::::::  -->
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="page-breadcrumb__menu">
                        <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                        <li class="page-breadcrumb__nav active">Halaman Checkout </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- ::::::  End  Breadcrumb Section  ::::::  -->

    <!-- ::::::  Start  Main Container Section  ::::::  -->
    <main id="main-container" class="main-container">
        <div class="container">
            <div class="row">
                <!-- Start Client Shipping Address -->
                <div class="col-lg-7">
                    <div class="section-content">
                        <h5 class="section-content__title">Detail Transaksi</h5>
                    </div>
                    <form action="{{route('customer.prosescheckout')}}" method="post" class="form-box">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-box__single-group">
                                    <label for="form-first-name">Nama</label>
                                    <input readonly type="text" id="form-first-name"  name="nama" 
                                        value="{{ Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-box__single-group">
                                    <label for="form-email">Alamat Email </label>
                                    <input readonly type="email" id="form-email" name="email" 
                                        value="{{ Auth::user()->email }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-box__single-group">
                                    <label for="form-telp">No Telp </label>
                                    <input readonly type="number" id="form-email" name="hp" placeholder="masukan no telp"
                                        value="{{ Auth::user()->telepon }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-box__single-group">
                                    <label for="form-address-1">Keterangan Pembelian</label>
                                    <textarea name="keterangan_pembelian" id="keterangan_pembelian" cols="20" placeholder="masukan ketarangan pembelian" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                </div> <!-- End Client Shipping Address -->

                <!-- Start Order Wrapper -->
                <div class="col-lg-5">
                    <div class="your-order-section">
                        <div class="section-content">
                            <h5 class="section-content__title">Pesanan Anda</h5>
                        </div>
                        <div class="your-order-box gray-bg m-t-40 m-b-30">
                            <div class="your-order-product-info">
                                <div class="your-order-top d-flex justify-content-between">
                                    <h6 class="your-order-top-left">Produk</h6>
                                    <h6 class="your-order-top-right">Total</h6>
                                </div>
                                <ul class="your-order-middle">
                                    @foreach ($keranjang as $item)
                                        <li class="d-flex justify-content-between">
                                            <span class="your-order-middle-left">{{ $item->name }} X
                                                {{ $item->quantity }}</span>
                                            <span class="your-order-middle-right">Rp.
                                                {{ number_format(\Cart::getSubtotal()) }}  </span>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                            <div class="form-group">
                                <label>Pilih Bank Tranfer</label>
                                <select class="custom-select" name="bank_id" id="selectBank">
                                    @foreach ($bank as $item)
                                        <option data-rekening="{{ $item->no_rekening }}" 
                                            value="{{ $item->id }}">{{ $item->nama_bank }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>No Rekening</label>
                                <input class="form-control" id="no_rekening" readonly />
                            </div>
                        </div>
                        <button type="submit" class="btn btn--block btn--small btn--blue btn--uppercase btn--weight">Proses</button>
                        </div>
                    </form>
                    <a href="{{url('/')}}" class="btn btn--small mt-2 btn--block btn-warning btn--uppercase btn--weight">Beranda</a>
                </div> <!-- End Order Wrapper -->
            </div>
        </div>
    </main> <!-- ::::::  End  Main Container Section  ::::::  -->
@endsection

@push('scripts')
    <script>
        $('body').on('change', '#selectBank', function() {
            var data = $(this).find(':selected').data('rekening');
            $("#no_rekening").val(data);
        });
    </script>
@endpush