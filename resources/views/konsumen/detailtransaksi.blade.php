@extends('konsumen.layouts.master')
@section('content')

    <!-- ::::::  Start  Breadcrumb Section  ::::::  -->
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="page-breadcrumb__menu">
                        <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                        <li class="page-breadcrumb__nav active">Akun saya</li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- ::::::  End  Breadcrumb Section  ::::::  -->

    <main id="main-container" class="main-container">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card card-body">
                        @if(session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <h4>Detail Transaksi</h4>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode</th>
                                            <th>Barang</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 0; $jumlah = 0; $totalHarga = 0; @endphp
                                        @foreach ($detTr as $item)
                                            @php $no++; 
                                                $jumlah = $jumlah + $item->qty; 
                                                $totalHarga = $totalHarga + $item->harga;
                                            @endphp
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $item->barang_kode }}</td>
                                                <td>{{ $item->barang_nama }}</td>
                                                <td>{{ $item->qty }}</td>
                                                <td>{{ number_format($item->harga,0,',','.') }}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="3">Total</td>
                                            <td>{{$jumlah}}</td>
                                            <td>{{number_format($totalHarga,0,',','.')}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                @if($tr->bukti_transfer != null)
                                    <div class="form-group">
                                        <label>Bukti Pembayaran</label>
                                        <img src="{{ asset('images/bukti_transfer/'.$tr->bukti_transfer) }}" class="img-bukti-transfer" />
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <h4>Form Upload</h4>
                                <form enctype="multipart/form-data" method="POST" action="{{ url('upload_bukti_bayar') }}">
                                    @csrf
                                    <input type="hidden" name="id_pembelian" value="{{ $tr->id }}" />
                                    <div class="form-group">
                                        <label>Bank</label>
                                        <input class="form-control" readonly value="{{$tr->nama_bank}}" />
                                    </div>
                                    <div class="form-group">
                                        <label>Rekening</label>
                                        <input class="form-control" readonly value="{{$tr->no_rekening}}" />
                                    </div>
                                    @if($tr->bukti_transfer == null || $tr->is_verified == 2)
                                        @if($tr->is_verified == 2)
                                        <div class="alert alert-danger">
                                            Bukti pembayaran ditolak. {{ $tr->keterangan }} 
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label>Bukti Transfer</label>
                                            <div class="custom-file">
                                                <input type="file" accept="image/*" name="buktiTransfer" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Pilih file</label>
                                            </div>
                                            @error('buktiTransfer')
                                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <button class="btn btn-primary btn-lg btn-block p-3" type="submit">Simpan</button>
                                    @elseif($tr->bukti_transfer != null && $tr->is_verified == 0) 
                                        <div class="alert alert-info">
                                            Bukti pembayaran sudah diunggah, status menunggu verifikasi 
                                        </div>
                                    @elseif($tr->bukti_transfer != null && $tr->is_verified == 1) 
                                        <div class="alert alert-success">
                                            Bukti pembayaran sudah diverifikasi 
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection