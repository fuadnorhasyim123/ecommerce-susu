@extends('konsumen.layouts.master')
@section('content')
      <!-- ::::::  Start  Breadcrumb Section  ::::::  -->
      <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="page-breadcrumb__menu">
                        <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                        <li class="page-breadcrumb__nav active">Login Page</li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- ::::::  End  Breadcrumb Section  ::::::  -->

    <!-- ::::::  Start  Main Container Section  ::::::  -->
    <main id="main-container" class="main-container">
        <div class="container">
            <div class="row">
               <div class="col-12">
                <!-- login area start -->
                <div class="login-register-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                                <div class="login-register-wrapper">
                                    <div class="login-register-tab-list nav">
                                        <a class="active" data-toggle="tab" href="#lg1">
                                            <h4>login</h4>
                                        </a>
                                        <a data-toggle="tab" href="#lg2">
                                            <h4>register</h4>
                                        </a>
                                    </div>
                                    <div class="tab-content">
                                        <div id="lg1" class="tab-pane active">
                                            <div class="login-form-container">
                                                <div class="login-register-form">
                                                    <form action="{{route('login')}}" method="post">
                                                        @csrf
                                                        <div class="form-box__single-group">
                                                            <input type="text" id="form-username" name="username" placeholder="Masukan username">
                                                        </div>
                                                        <div class="form-box__single-group">
                                                            <input type="password" name="password" id="form-username-password" placeholder="Massukan Password">
                                                        </div>
                                                        <button class="btn btn--box btn--small btn--blue mt-3 btn--uppercase btn--weight btn-block" type="submit">MASUK</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="lg2" class="tab-pane">
                                            <div class="login-form-container">
                                                <div class="login-register-form">
                                                    <form action="{{route('register')}}" method="post">
                                                        @csrf
                                                        <div class="form-box__single-group">
                                                            <input type="text" name="name" id="form-register-username" placeholder="Masukan Username">
                                                        </div>
                                                        <div class="form-box__single-group">
                                                            <input type="email" name="email" id="form-uregister-sername-email" placeholder="Masukan email">
                                                        </div>
                                                        <div class="form-box__single-group m-b-20">
                                                            <input type="password" name="password" id="form-register-username-password" placeholder="Masukan password">
                                                        </div>
                                                        <div class="form-box__single-group m-b-20">
                                                            <input type="password" name="password_confirmation" id="form-register-username-password" placeholder="Masukan konfirmasi password">
                                                        </div>
                                                        <button class="btn btn--box btn--small btn-block btn--blue btn--uppercase btn--weight" type="submit">DAFTAR</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- login area end -->
               </div>
            </div>
        </div>
    </main> <!-- ::::::  End  Main Container Section  ::::::  -->

@endsection
