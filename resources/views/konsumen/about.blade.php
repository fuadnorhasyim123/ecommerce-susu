@extends('konsumen.layouts.master')
@section('content')
<!-- ::::::  Start  Breadcrumb Section  ::::::  -->
<!-- <div class="page-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="page-breadcrumb__menu">
                    <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                    <li class="page-breadcrumb__nav active">About</li>
                </ul>
            </div>
        </div>
    </div>
</div> -->
<!-- ::::::  End  Breadcrumb Section  ::::::  -->

<!-- ::::::  Start  Main Container Section  ::::::  -->
<main id="main-container" class="main-container">
    <div class="container">
        <div class="row justify-content-center p-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4>Toko Susu!</h4>
                        <p>Produk murah dengan kualitas terbaik, nikmati manfaat minum susu dengan memesan susu terbaik
                            melalui toko online ini.
                        </p>
                        <p><i class="fa fa-location"></i> Dusun Samar, Desa Mulyosari, Kec. Pagerwojo, Kab. Tulungagung</p>
                        <p><i class="fa fa-phone"></i> 082335876013</p>
                        <p><i class="fa fa-envelope"></i> tokosusu@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection