@extends('konsumen.layouts.master')
@section('content')
<!-- ::::::  Start  Breadcrumb Section  ::::::  -->
<!-- <div class="page-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="page-breadcrumb__menu">
                    <li class="page-breadcrumb__nav"><a href="#">Home</a></li>
                    <li class="page-breadcrumb__nav active">Chat Page</li>
                </ul>
            </div>
        </div>
    </div>
</div> -->
<!-- ::::::  End  Breadcrumb Section  ::::::  -->

<!-- ::::::  Start  Main Container Section  ::::::  -->
<main id="main-container" class="main-container">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <div class="section-content">
                    <h5 class="section-content__title">Chat Anda</h5>
                </div>
                <!-- Start Cart Table -->
                <div class="table-content cart-table-content m-t-30">
                    <div class="chat-container" id="loadMessage"></div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="messageUser" placeholder="Ketik pesan disini...">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="btnSendMessage">
                                        <span class="fa fa-paper-plane"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Cart Table -->
            </div>
        </div>
    </div>
</main> <!-- ::::::  End  Main Container Section  ::::::  -->
@endsection

@push('scripts')
<script>
    setInterval(() => loadMessage(), 3000);

    function loadMessage() {
        $.ajax({
            url: '{{ url("load-message") }}',
            type: 'GET',
            success: function(result) {
                let contentMessage = "";
                let userId = '{{ Auth::user()->id }}';
                console.log(userId);
                result.data.forEach(row => {
                    console.log("sender " + row.sender_id);
                    console.log("userId " + userId);
                    if (row.sender_id == userId) {
                        contentMessage += `<div class="row mb-3">
                                    <div class="col-6"></div>
                                    <div class="col-6">
                                        <div class="alert alert-success p-2 mb-1">${row.pesan}</div>
                                        <small>${tanggalIndo(row.created_at)} ${waktuFormat(row.created_at)}</small>
                                    </div>
                                </div>`;
                    } else {
                        contentMessage += `<div class="row mb-3">
                                <div class="col-6">
                                    <div class="alert alert-warning p-2 mb-1">
                                        <strong>${row.receiver_id == userId ? row.sender : row.receiver}</strong>
                                        <p>${row.pesan}</p>
                                    </div>
                                    <small>${tanggalIndo(row.created_at)} ${waktuFormat(row.created_at)}</small>
                                </div>
                                <div class="col-6"></div>
                            </div>`;
                    }
                });
                $("#loadMessage").html(contentMessage);
            }
        });
    }

    $("#btnSendMessage").on("click", function() {
        sendMessage();
    });

    var messageUser = document.getElementById("messageUser");
    messageUser.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            sendMessage();
        }
    });

    function sendMessage() {
        var message = $("#messageUser").val();
        $.ajax({
            url: '{{ url("send-message") }}',
            type: 'POST',
            data: {
                message: message
            },
            success: function(res) {
                loadMessage();
                $("#messageUser").val("");
            }
        })
    }

    function tanggalIndo(tgl) {
        let date = new Date(tgl);
        let tahun = date.getFullYear();
        let bulan = date.getMonth();
        let tanggal = date.getDate();
        let hari = date.getDay();
        let listHari = [
            "Minggu",
            "Senin",
            "Selasa",
            "Rabu",
            "Kamis",
            "Jum'at",
            "sabtu",
        ];
        listBulan = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
        ];
        return tanggal + " " + listBulan[bulan] + " " + tahun;
    }

    function waktuFormat(tgl) {
        let date = new Date(tgl);
        let jam = date.getHours();
        let menit = date.getMinutes();
        let second = date.getSeconds();
        return jam + ":" + menit + ":" + second;
    }
</script>
@endpush