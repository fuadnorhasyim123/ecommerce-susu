@extends('konsumen.layouts.master')
@section('content')
    <main id="main-container" class="main-container">

        <div class="product product--1 swiper-outside-arrow-hover">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div
                            class="section-content section-content--border d-flex align-items-center justify-content-between">
                            <h5 class="section-content__title">Daftar Produk </h5>
                            <div class="col-6">
                                <form action="{{ url('produk') }}" method="GET">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="search"
                                            placeholder="Cari produk susu...">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="submit" id="btnSendMessage">
                                                <span class="fa fa-search"></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    @foreach ($produk as $item)
                        <div class="col-md-4">
                            <div
                                class="product__box product__box--default product__box--border-hover swiper-slide text-center">
                                <div class="product__img-box">
                                    <a href="{{ route('customer.detailproduk', $item->barang_id) }}"
                                        class="product__img--link">
                                        <img class="product__img" src="{{ asset('images/' . $item->barang_gambar) }}"
                                            alt="">
                                    </a>
                                </div>
                                <div class="product__price m-t-10">
                                    <span class="product__price-reg">Rp {{ number_format($item->barang_harga, 0) }}</span>
                                </div>
                                Sisa : {{ $item->barang_stok }}
                                <a href="{{ route('customer.detailproduk', $item->barang_id) }}"
                                    class="product__link product__link--underline product__link--weight-light m-t-15">
                                    {{ $item->barang_nama }}
                                </a>
                            </div> <!-- End Single Default Product -->
                        </div>
                    @endforeach
                </div>
                {{ $produk->links() }}
            </div>
        </div>

    </main>
@endsection
