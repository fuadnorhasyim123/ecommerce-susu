@extends('konsumen.layouts.master')
@section('content')
<!-- ::::::  Start  Breadcrumb Section  ::::::  -->
<div class="page-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="page-breadcrumb__menu">
                    <li class="page-breadcrumb__nav"><a href="#">Beranda</a></li>
                    <li class="page-breadcrumb__nav active">Keranjang</li>
                </ul>
            </div>
        </div>
    </div>
</div> <!-- ::::::  End  Breadcrumb Section  ::::::  -->

<!-- ::::::  Start  Main Container Section  ::::::  -->
<main id="main-container" class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-content">
                    <h5 class="section-content__title">Keranjang Anda</h5>
                </div>
                <!-- Start Cart Table -->
                <div class="table-content table-responsive cart-table-content m-t-30">
                    <table>
                        <thead class="gray-bg">
                            <tr>
                                <th>Gambar</th>
                                <th>Nama Barang</th>
                                <th>Harga</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form action="{{ route('customer.updatekeranjang') }}" method="POST">
                                @csrf
                                @foreach ($cart->getContent() as $item)
                                <input type="hidden" name="id[]" value="{{ $item->id }}" />
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#">
                                            <img class="img-fluid" src="{{ asset('images/' . $item->attributes->image) }}" alt=""></a>
                                    </td>
                                    <td class="product-name"><a href="#">{{ $item->name }}</a></td>
                                    <td class="product-price-cart"><span class="amount">Rp.
                                            {{ number_format($item->price, 0) }}</span></td>
                                    <td class="product-quantities">

                                        <div class="quantity d-inline-block">
                                            <input type="number" name="qty[]" min="1" step="1" value="{{ $item->quantity }}">
                                        </div>
                                    </td>
                                    <td class="product-subtotal">Rp.
                                        {{ number_format($item->price * $item->quantity) }}
                                    </td>
                                    <td class="product-remove">
                                        <a title="Hapus Keranjang" href="{{ route('customer.deletekeranjang', $item->id) }}"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                @endforeach

                        </tbody>
                    </table>
                </div> <!-- End Cart Table -->
                <!-- Start Cart Table Button -->
                <div class="cart-table-button m-t-10">
                    <div class="cart-table-button--left">
                        <a href="{{ url('produk') }}" class="btn btn--box btn--large btn--gray btn--uppercase btn--weight m-t-20">LANJUT BELANJA</a>
                    </div>
                    <div class="cart-table-button--right">
                        <button type="submit" class="btn btn--box btn--large btn--gray btn--uppercase btn--weight m-t-20">Ubah
                            Keranjang</button>
                        </form>
                        <a href="{{ route('customer.checkout') }}" class="btn btn--box btn--large btn--blue btn--uppercase btn--weight m-t-20">Selesai Belanja</a>
                    </div>
                </div> <!-- End Cart Table Button -->
            </div>

        </div>
    </div>
</main> <!-- ::::::  End  Main Container Section  ::::::  -->
@endsection