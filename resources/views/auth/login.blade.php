@extends('konsumen.layouts.master')
@section('content')

<!-- ::::::  Start  Breadcrumb Section  ::::::  -->
<div class="page-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="page-breadcrumb__menu">
                    <li class="page-breadcrumb__nav"><a href="{{route('customer.dashboard')}}">Home</a></li>
                    <li class="page-breadcrumb__nav active">Login Page</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <!-- login area start -->
            <div class="login-register-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                            <div class="login-register-wrapper">
                                <div class="login-register-tab-list nav">
                                    <a class="active" data-toggle="tab" href="#lg1">
                                        <h4>login</h4>
                                    </a>
                                    <a data-toggle="tab" href="#lg2">
                                        <h4>register</h4>
                                    </a>
                                </div>
                                @include('sweetalert::alert')
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Periksa Kembali!</strong> Ada kesalahan input data.
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="tab-content">
                                    <div id="lg1" class="tab-pane active">
                                        <div class="login-form-container">
                                            <div class="login-register-form">
                                                <form role="form" action="{{route('login')}}" method="POST">
                                                    @csrf
                                                    <div class="form-box__single-group">
                                                        <label>Username</label>
                                                        <input type="text" id="form-username" name="username" placeholder="Username" required>
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Kata Sandi</label>
                                                        <input type="password" id="form-username-password" name="password" placeholder="Enter kata sandi" required>
                                                    </div>
                                                    <div class="d-flex justify-content-between flex-wrap m-tb-20">
                                                    </div>
                                                    <button class="btn btn--box btn--small btn--blue btn--uppercase btn--weight" type="submit">MASUK</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="lg2" class="tab-pane">
                                        <div class="login-form-container">
                                            <div class="login-register-form">
                                                <form method="POST" action="{{ route('register') }}">
                                                    @csrf
                                                    <div class="form-box__single-group">
                                                        <label>Nama</label>
                                                        <input type="text" name="name" id="form-register-username" required placeholder="Masukkan Nama">
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Username</label>
                                                        <input type="text" name="username" id="form-register-username" required placeholder="Masukkan Username">
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Email</label>
                                                        <input type="email" name="email" id="form-uregister-sername-email" required placeholder="Masukkan Email">
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Telepon</label>
                                                        <input type="text" name="telepon" id="form-uregister-sername-email" required placeholder="Masukkan telepon">
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Provinsi</label>
                                                        <select onChange="pilihProvinsi()" name="provinsi_id" id='select_provinsi'></select>
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Kota</label>
                                                        <select id='select_kota' name="kota_id" ></select>
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Kode Pos</label>
                                                        <input type="number" name="kode_pos" id="form-uregister-sername-email" required placeholder="Masukkan Kode POS">
                                                    </div>
                                                    <div class="form-box__single-group">
                                                        <label>Alamat</label>
                                                        <textarea class="form-control" name="alamat" placeholder="Enter Alamat" required></textarea>
                                                    </div>
                                                    <div class="form-box__single-group m-b-20">
                                                        <label>Kata Sandi</label>
                                                        <input type="password" name="password" id="form-register-username-password" required placeholder="Masukkan Kata Sandi">
                                                    </div>
                                                    <div class="form-box__single-group m-b-20">
                                                        <label>Konfirmasi Kata Sandi</label>
                                                        <input type="password" name="password_confirmation" id="form-register-username-password" required placeholder="Masukkan Ulang Kata Sandi">
                                                    </div>
                                                    <button class="btn btn--box btn--small btn--blue btn--uppercase btn--weight" type="submit">DAFTAR</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login area end -->
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
            $("#select_provinsi").select2({
                ajax: { 
                    url: "{{ url('get_provinsi') }}",
                    type: "GET",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                    return {
                        search: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            function pilihProvinsi() {
                var provinsi = $("#select_provinsi").val();
                console.log(provinsi);
                loadKota(provinsi);
            }

            function loadKota(provinsiId) {
                $("#select_kota").select2({
                    ajax: { 
                        url: "{{ url('get_kota') }}"+"?provinsi="+provinsiId,
                        type: "GET",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                        return {
                            search: params.term // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: true
                    }
                });
            }
    </script>
@endpush