@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <h4 class="card-title">About Us</h4>
                        </div>
                    </div>
                    <h4 class="text-center">Toko Susu!</h4>
                    <p class="text-center">Produk murah dengan kualitas terbaik, nikmati manfaat minum susu dengan memesan susu terbaik
                        melalui toko online ini.
                    </p>
                    <p class="text-center">Dusun Samar, Desa Mulyosari, Kec. Pagerwojo, Kab. Tulungagung
                        <br> Telp. : 082335876013
                        <br> Email : tokosusu@gmail.com
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection