@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                    <h3 class="font-weight-bold">Welcome {{Auth::user()->name}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin transparent">
            <div class="row">
                <div class="col-md-6 mb-4 stretch-card transparent">
                    <div class="card card-tale">
                        <div class="card-body">
                            <p class="mb-4">Total Customer</p>
                            <p class="fs-30 mb-2">{{$tcust}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-4 stretch-card transparent">
                    <div class="card card-dark-blue">
                        <div class="card-body">
                            <p class="mb-4">Total Barang</p>
                            <p class="fs-30 mb-2">{{$tbrg}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-4 mb-lg-0 stretch-card transparent">
                    <div class="card card-light-blue">
                        <div class="card-body">
                            <p class="mb-4">Total Transaksi</p>
                            <p class="fs-30 mb-2">{{$ttrx}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card transparent">
                    <div class="card card-light-danger">
                        <div class="card-body">
                            <p class="mb-4">Total Transaksi Selesai</p>
                            <p class="fs-30 mb-2">{{$ttrxsls}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <p class="card-title">Grafik Transaksi Berdasarkan Barang</p>
                </div>
                <div id="chart1"></div>
            </div>
        </div>
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <p class="card-title">Grafik Transaksi Berdasarkan Customer</p>
                    </div>
                    <div id="chart2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    //chart1
    google.charts.load("current", {
        packages: ["corechart"]
    });

    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawChart1);


    //chart
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Produk', 'Total'],
            <?php
            foreach ($trx_produk as $data) {
                echo "['" . $data->barang_nama .
                    "', " . $data->total .
                    "],";
            };
            ?>
        ]);

        var options = {
            title: '',
            width: 400,
            height: 250,
            is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart1'));
        chart.draw(data, options);
    }

    function drawChart1() {
        var data = google.visualization.arrayToDataTable([
            ['Produk', 'Total'],

            <?php
            foreach ($trx_user as $data) {
                echo "['" . $data->provinsi .
                    "', " . $data->total .
                    "],";
            };
            ?>
        ]);

        var options = {
            title: '',
            width: 400,
            height: 225,
            bar: {
                groupWidth: "80%"
            },
            legend: {
                position: "right"
            },
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart2'));
        chart.draw(data, options);
    }
</script>
@endpush