@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <h4 class="card-title">Data Laporan Penjualan</h4>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">
                            <input type="date" class="form-control" id="tgl_awal" name="tgl_awal" required>
                        </div>
                        <p class="mt-2">Sampai</p>
                        <div class="col-3">
                            <input type="date" class="form-control" id="tgl_akhir" name="tgl_akhir" required>
                        </div>
                        <div class="col-4">
                            <button id="filter" class="btn btn-primary"><i class="mdi mdi-filter"></i> Filter</button>
                        </div>
                    </div>
                    <table id="table" class="table table-bordered" style="width: 100%;">
                        <thead class="table table-dark">
                            <tr>
                                <th>No.</th>
                                <th>ID Penjualan</th>
                                <th>Tanggal</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Bank</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="7">TOTAL</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- row end -->
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            "scrollX": true,
            "paging": false,
            "searching": false,
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excel',
                footer: true
            }, {
                extend: 'pdf',
                footer: true
            }],

            ajax: {
                url: "{{ route('admin.laporan-pembelian') }}",
                data: function(d) {
                    d.tgl_awal = $('#tgl_awal').val(),
                        d.tgl_akhir = $('#tgl_akhir').val()
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'alamat',
                    name: 'alamat'
                },
                {
                    data: 'nama_bank',
                    name: 'nama_bank'
                },
                {
                    data: 'total_tr',
                    name: 'total_tr',
                    render: $.fn.dataTable.render.number(',', '.', 2, '')
                },
            ],
            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;


                // converting to interger to find total
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // computing column Total of the complete result 
                var total = api
                    .column(7)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer by showing the total with the reference of the column index 
                $(api.column(7).footer()).html(format(total));
            },
        });

        $("#filter").click(function() {
            table.draw();
        });
    });
</script>
@endpush