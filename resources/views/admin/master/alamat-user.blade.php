@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <h4 class="card-title">Data Alamat Konsumen</h4>
                        </div>
                    </div>
                    <div class="row mb-3">
                    </div>
                    <table id="table" class="table table-bordered" style="width: 100%;">
                        <thead class="table table-dark">
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Alamat</th>
                                <th>Kota</th>
                                <th>Provinsi</th>
                                <th>Kode Pos</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- row end -->
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            "scrollX": true,
            "paging": false,
            "searching": false,
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excel',
                footer: true
            }, {
                extend: 'pdf',
                footer: true
            }],
            ajax: {
                url: "{{ route('admin.alamat-user') }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'username',
                    name: 'username'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'alamat',
                    name: 'alamat'
                },
                {
                    data: 'kota',
                    name: 'kota'
                },
                {
                    data: 'provinsi',
                    name: 'provinsi'
                },
                {
                    data: 'kode_pos',
                    name: 'kode_pos'
                },
            ],
        });
    });
</script>
@endpush