@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <h4 class="card-title">Data Pengiriman</h4>
                        </div>
                    </div>
                    <table id="table" class="table table-striped table-hover" width="100%">
                        <thead class="table-dark">
                            <tr>
                                <th>No.</th>
                                <th>ID Pembelian</th>
                                <th>Tanggal Pembelian</th>
                                <th>Status Dikirim</th>
                                <th>Status Sampai</th>
                                <th>Tanggal Pengiriman</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden=" true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mt-1">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-control-label">Kode Pembelian :</label>
                            <input type="text" class="form-control" id="kode_tr1" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-control-label">Nama Pembeli :</label>
                            <input type="text" class="form-control" id="name1" readonly>
                        </div>
                    </div>
                </div>
                <div class="row mt-1">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-control-label">Email :</label>
                            <input type="email" class="form-control" id="email1" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-control-label">No. Telp :</label>
                            <input type="number" class="form-control" id="telepon1" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-control-label">Alamat Pembeli :</label>
                            <input type="text" class="form-control" id="alamat1" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-control-label">Total :</label>
                            <input type="text" class="form-control" id="total_tr1" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal verif -->
<div class="modal fade bd-example-modal-lg" id="kirim" tabindex="-1" role="dialog" aria-labelledby="kirimLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="kirimLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.pengiriman-kirim')}}" method="POST">
                @csrf
                <input type="hidden" name="id2" id="id2">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            "scrollX": true,
            ajax: "{{ route('admin.pengiriman') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'id_pembelian',
                    name: 'id_pembelian'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'sk',
                    name: 'sk',
                    searchable: false
                },
                {
                    data: 'ss',
                    name: 'ss',
                    searchable: false
                },
                {
                    data: 'tanggal_pengiriman',
                    name: 'tanggal_pengiriman'
                },
            ]
        });
    });

    $(document).ready(function() {

        // modal kirim
        $('body').on('click', '#btnKirim', function() {
            var data_id = $(this).data('id');
            $.get('pengiriman/' + data_id + '/edit', function(data) {
                $('#kirimLabel').html("Kirim Pembelian");
                $('#btn-save').prop('disabled', false);
                $('#kirim').modal('show');
                $('#id2').val(data.id);
            })
        });
    });
</script>
@endpush