@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <h4 class="card-title">Daftar Chat Customer</h4>
                        </div>
                        <div class="col-6 text-md-right">
                            <a href="javascript:void(0)" id="btnTambah" class="btn btn-primary btn-icon-text">
                                <i class="mdi mdi-plus-box"></i>
                                Kirim Pesan
                            </a>
                        </div>
                    </div>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <table id="table" class="table table-striped table-hover" width="100%">
                        <thead class="table-dark">
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden=" true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-content cart-table-content m-t-30">
                            <div class="chat-container" id="loadMessage"></div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="input-group mb-3">
                                        <input type="hidden" id="user_id">
                                        <input type="text" class="form-control" id="messageUser" placeholder="Ketik pesan disini...">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" id="btnSendMessage">
                                                <span class="mdi mdi-send"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden=" true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.chat-tambah')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class=" form-control-label">Pilih Username Tujuan :</label>
                                <select name="sender_id" id="sender_id" class="js-example-basic-single w-100" style="width: 100%;" required>
                                    <option value="" selected>---</option>
                                    @foreach($customer as $data)
                                    <option value="{{$data->id}}">{{$data->username}} - {{$data->alamat}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class=" form-control-label">Pesan :</label>
                                <textarea id="message" name="message" placeholder="Masukkan Pesan" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal tambah & edit -->
@endsection
@push('scripts')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            "scrollX": true,
            ajax: "{{ route('admin.chat') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'username',
                    name: 'username'
                },
                {
                    data: 'alamat',
                    name: 'alamat'
                },
                {
                    data: 'telepon',
                    name: 'telepon'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    searchable: false
                },
            ]
        });
    });

    $(document).ready(function() {

        // modal detail
        $('body').on('click', '#btnDetail', function() {
            var data_id = $(this).data('id');
            $.get('chat/' + data_id + '/edit/', function(data) {
                $('#detailLabel').html("Chat dari " + data.username);
                $('#detail').modal('show');
                loadMessage(data.id);
                $('#user_id').val(data.id);
            })
        });

        // modal tambah
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Kirim Pesan");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#id').val("");
            $('#sender_id').val("").trigger('change');
            $('#message').val("");

        });

    });
</script>

<script>
    // setInterval(() => loadMessage(), 3000);

    function loadMessage(id) {
        $.ajax({
            url: 'load-message/' + id,
            type: 'GET',
            success: function(result) {
                let contentMessage = "";
                let userId = '{{ Auth::user()->id }}';
                console.log(result);
                result.data.forEach(row => {
                    // console.log("userId " + userId);
                    if (row.sender_id == userId) {
                        contentMessage += `<div class="row mb-3">
                                    <div class="col-6"></div>
                                    <div class="col-6">
                                        <div class="alert alert-success p-2 mb-1">${row.pesan}</div>
                                        <small>${tanggalIndo(row.created_at)} ${waktuFormat(row.created_at)}</small>
                                    </div>
                                </div>`;
                    } else {
                        contentMessage += `<div class="row mb-3">
                                <div class="col-6">
                                    <div class="alert alert-warning p-2 mb-1">
                                        <strong>${row.receiver_id == userId ? row.sender : row.receiver}</strong>
                                        <p>${row.pesan}</p>
                                    </div>
                                    <small>${tanggalIndo(row.created_at)} ${waktuFormat(row.created_at)}</small>
                                </div>
                                <div class="col-6"></div>
                            </div>`;
                    }
                });
                $("#loadMessage").html(contentMessage);
            }
        });
    }

    $("#btnSendMessage").on("click", function() {
        sendMessage();
    });

    var messageUser = document.getElementById("messageUser");
    messageUser.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            sendMessage();
        }
    });

    function sendMessage() {
        var message = $("#messageUser").val();
        $.ajax({
            url: '{{ url("admin/send-message") }}',
            type: 'POST',
            data: {
                message: message,
                id: $('#user_id').val()
            },
            success: function(res) {
                loadMessage($('#user_id').val());
                $("#messageUser").val("");
            }
        })
    }

    function tanggalIndo(tgl) {
        let date = new Date(tgl);
        let tahun = date.getFullYear();
        let bulan = date.getMonth();
        let tanggal = date.getDate();
        let hari = date.getDay();
        let listHari = [
            "Minggu",
            "Senin",
            "Selasa",
            "Rabu",
            "Kamis",
            "Jum'at",
            "sabtu",
        ];
        listBulan = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
        ];
        return tanggal + " " + listBulan[bulan] + " " + tahun;
    }

    function waktuFormat(tgl) {
        let date = new Date(tgl);
        let jam = date.getHours();
        let menit = date.getMinutes();
        let second = date.getSeconds();
        return jam + ":" + menit + ":" + second;
    }
</script>
@endpush