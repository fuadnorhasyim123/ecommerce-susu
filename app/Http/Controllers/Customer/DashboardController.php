<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\DetailPembelian;
use App\Models\BuktiTransfer;
use App\Models\Pengiriman;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function index()
    {
        $userId = Auth::user();
        if ($userId !== null) {
            $cart = \Cart::session(Auth::user()->id)->getContent();
        } else {
            $cart = \Cart::getContent();
        }
        $data = [
            'produk' => DB::table('barang')->where('barang_status', 1)->paginate(10),
            'cart' => $cart
        ];
        return view('konsumen.home', $data);
    }

    public function detailproduk($id)
    {
        $data = DB::table('barang')->where('barang_id', $id)->first();
        return view('konsumen.detailproduk', compact('data'));
    }

    public function myAccount()
    {
        $data = [
            'akun' => DB::table('users')->where('id', Auth::user()->id)->first(),
            'pesanan' => DB::table('pembelian as tr')
                ->join('bukti_transfer as tf', 'tf.id_pembelian', '=', 'tr.id')
                ->join('pembayaran as byr', 'byr.id_pembelian', '=', 'tr.id')
                ->join('bank as b', 'b.id', '=', 'byr.bank_id')
                ->select(
                    'tr.id',
                    'tr.id_konsumen',
                    'tr.created_at',
                    'tf.bukti_transfer',
                    'tf.is_verified',
                    'tr.total_tr'
                )
                ->where('tr.id_konsumen', Auth::user()->id)
                ->orderBy('tf.is_verified', 'ASC')
                ->get(),
            'provinsi' => DB::table('provinces')->select('province_id as id', 'name as text')->get(),
            'kota' => DB::table('cities')->select('city_id as id', 'name as text')->get(),
            'pengiriman' => DB::table('pembelian as tr')->join('pengiriman as krm', 'krm.id_pembelian', '=', 'tr.id')
                ->where('tr.id_konsumen', Auth::user()->id)
                ->select('tr.id', 'tr.id_konsumen', 'krm.status_dikirim', 'krm.status_sampai', 'tr.created_at', 'krm.tanggal_pengiriman')
                ->get(),
            'belumBayar' => DB::table('pembelian as tr')
                ->join('bukti_transfer as tf', 'tf.id_pembelian', '=', 'tr.id')
                ->join('pembayaran as byr', 'byr.id_pembelian', '=', 'tr.id')
                ->join('bank as b', 'b.id', '=', 'byr.bank_id')
                ->where('tr.id_konsumen', Auth::user()->id)
                ->whereNull('tf.bukti_transfer')
                ->where('tf.is_verified', 0)
                ->count(),
        ];
        return view('konsumen.myAccount', ['data' => $data]);
    }

    public function detailTransaksi($id)
    {
        $tr = DB::table('pembelian as tr')
            ->join('bukti_transfer as tf', 'tf.id_pembelian', '=', 'tr.id')
            ->join('pembayaran as byr', 'byr.id_pembelian', '=', 'tr.id')
            ->join('bank as b', 'b.id', '=', 'byr.bank_id')
            ->select(
                'tr.id',
                'tr.id_konsumen',
                'b.nama_bank',
                'b.no_rekening',
                'tr.created_at',
                'tf.bukti_transfer',
                'tf.is_verified',
                'tr.total_tr',
                'tf.keterangan'
            )
            ->where('tr.id_konsumen', Auth::user()->id)
            ->where('tr.id', $id)
            ->orderBy('tr.created_at', 'DESC')
            ->first();
        $detTr = DetailPembelian::join('pembelian', 'detail_pembelian.pembelian_id', '=', 'pembelian.id')
            ->join('barang', 'detail_pembelian.id_barang', '=', 'barang.barang_id')
            ->where('detail_pembelian.pembelian_id', $id)
            ->select(
                'barang.barang_nama',
                'barang.barang_kode',
                'barang.barang_satuan',
                'detail_pembelian.harga',
                'detail_pembelian.qty'
            )
            ->get();
        return view('konsumen.detailtransaksi', compact('tr', 'detTr'));
    }

    public function uploadBuktiTransfer(Request $request)
    {
        $validatedData = $request->validate([
            'buktiTransfer' => 'required|image|mimes:jpg,png,jpeg|max:6000',
        ]);
        $imageName = time() . $request->file('buktiTransfer')->getClientOriginalName();
        $request->buktiTransfer->move(public_path('images/bukti_transfer'), $imageName);
        $oldStatus = BuktiTransfer::where('id_pembelian', $request->id_pembelian)->first();
        if($oldStatus->is_verified == 2){
            BuktiTransfer::where('id_pembelian', $request->id_pembelian)
                ->update([
                    'bukti_transfer' => $imageName,
                    'is_verified' => 0
                ]);
        }else{
            BuktiTransfer::where('id_pembelian', $request->id_pembelian)
                ->update([
                    'bukti_transfer' => $imageName,
                ]);
        }
      
        return redirect()->back()->with('status', 'Berhasil melakukan upload pembayaran, menunggu verifikasi');
    }

    public function updateStatusSampai(Request $request)
    {
        Pengiriman::where('id_pembelian', $request->id_pembelian)
            ->update([
                'status_sampai' => 1
            ]);

        DB::table('pembelian')->where('id', $request->id_pembelian)->update([
            'status'    => '1'
        ]);

        return redirect()->back()->with('status', 'Barang telah sampai');
    }

    public function updateAlamat(Request $request)
    {
        $current_password = $request->current_password;
        $password_baru = $request->password_baru;
        $password_konfirmasi = $request->password_konfirmasi;

        if ($password_baru != $password_konfirmasi) {
            return redirect()->back()->with('status', 'Password Baru dengan Konfirmasi Password Tidak Sama');
        }else{
            $user = User::find(\Auth::user()->id);
            if (Hash::check($current_password, $user->password)) {
                $user->password = Hash::make($request->password_baru);
                $user->save();
                return redirect()->back()->with('status', 'Password berhasil diubah');
            } else {
                return redirect()->back()->with('status', 'Password anda tidak cocok dengan password anda login');
            }
        }
    }

    public function updateProfil(Request $request)
    {
        $user = User::find(\Auth::user()->id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->telepon = $request->telepon;
            $user->alamat = $request->alamat;
            $user->provinsi_id = $request->provinsi_id;
            $user->kota_id = $request->kota_id;
            $user->kode_pos = $request->kode_pos;
            $user->save();
            return redirect()->back()->with('status', 'Berhasil mengubah data user');
    }
}
