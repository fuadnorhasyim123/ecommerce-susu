<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $produk = DB::table('barang')->where('barang_nama', 'like', '%' . $search . '%')->where('barang_status', '1')->paginate(10);
        return view('konsumen.produk', compact('produk'));
    }
}
