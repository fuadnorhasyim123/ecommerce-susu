<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat;
use App\Models\User;

class ChatController extends Controller
{
    
    public function index()
    {
        return view('konsumen.chat');
    }

    public function loadMessage()
    {
        Chat::where('receiver_id', \Auth::user()->id)
            ->update([
                'is_read_by_receiver' => 1
            ]);
        $chats = Chat::join('users as sender', 'sender.id', '=', 'chat.sender_id')
            ->join('users as receiver', 'receiver.id', '=', 'chat.receiver_id')
            ->where('chat.sender_id', \Auth::user()->id)
            ->orWhere('chat.receiver_id', \Auth::user()->id)
            ->select('chat.id', 'chat.message as pesan', 'chat.sender_id', 'chat.receiver_id',
                'sender.name as sender', 'receiver.name as receiver', 'chat.created_at')
            ->get();
        
        return response()->json(["code" => 200, "data" => $chats]);
    }

    public function sendMessage(Request $request)
    {
        $user = User::where('role', 'admin')->select('id')->first();
        Chat::create([
            'sender_id' => \Auth::user()->id,
            'receiver_id' => $user->id,
            'message' => $request->message,
            'is_read_by_sender' => 0,
            'is_read_by_receiver' => 1
        ]);
        return response()->json(["code" => 200, "message" => "Berhasil kirim pesan"]);
    }

    public function cekUnReadMessage()
    {
        $count = Chat::where('receiver_id', \Auth::user()->id)
            ->where('is_read_by_receiver', 0)->count();
        return response()->json(["code" => 200, "data" => $count]);
    }

}
