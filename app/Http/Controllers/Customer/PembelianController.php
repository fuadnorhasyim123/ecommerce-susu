<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\Pembelian;
use App\Models\Bank;
use App\Models\DetailPembelian;
use App\Models\BuktiTransfer;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PembelianController extends Controller
{
    public function keranjang()
    {
        $userId = auth()->user()->id;
        $cart = \Cart::session($userId);
        // dd($cart);
        return view('konsumen.cart', compact('cart'));
    }
    public function tambahkeranjang(Request $request, $id)
    {
        if (Auth::user() == null) {
            toast()->error('Gagal Menambah', 'Anda Harus Login terlebih dahulu')->autoclose(5000);
            return redirect()->back();
        }
        $userId = auth()->user()->id;
        $produk = Barang::find($id);
        $cek = \Cart::get($id);
        $qty = 1;
        if(isset($request->qty)){
            $qty = $request->qty;
        }
        if ($cek == null) {
            \Cart::session($userId)->add([
                'id' => $id,
                'name' => $produk->barang_nama,
                'price' => $produk->barang_harga,
                'quantity' => $qty,
                'attributes' => array(
                    'image' => $produk->barang_gambar
                )
            ]);
        } else {
            \Cart::update($id, ['quantity' => $cek->quantity++]);
        }
        alert()->success('Sukses', 'Barang Berhasil ditambahkan kedalam keranjang')->autoclose(5000);
        return redirect()->back();
    }

    public function updatekeranjang(Request $request)
    {
        $userId = auth()->user()->id;
        $id = $request->id;
        $qty = $request->qty;
        for ($i = 0; $i < count($request->id); $i++) {
            \Cart::session($userId)->update($id[$i], [
                'quantity' => [
                    'relative' => false,
                    'value' => $qty[$i]
                ]
            ]);
        }
        alert()->success('Keranjang berhasil diupdate', 'Sukses')->autoclose(3000);
        return redirect()->back()->with('success_msg', 'Cart is Updated!');
    }
    public function deletekeranjang($id)
    {
        $userId = auth()->user()->id;
        \Cart::session($userId)->remove($id);
        alert()->success('Barang Berhasil dihapus dalam keranjang', 'Sukses dihapus')->autoclose(5000);
        return redirect()->back();
    }

    public function checkout()
    {
        $userId = auth()->user()->id;
        $data = [
            'keranjang' => \Cart::session($userId)->getContent(),
            'bank' => Bank::all()
        ];

        // $total = \Cart::session($userId)->getTotal();
        // // dd($cart);
        // foreach($cart as $i){
        //     $today = date("Ymd");
        //     $rand = strtoupper(substr(uniqid(time()),0,4));
        //     $transaksi = new Pembelian();
        //     $transaksi->kd_pembelian = $today.$rand;
        //     $transaksi->id_konsumen = Auth::user()->id;
        //     $transaksi->nama_produk = $i->name;
        //     $transaksi->harga_produk = $i->price;
        //     $transaksi->jumlah_produk = $i->quantity;
        //     $transaksi->keterangan_pembelian = 0;
        //     $transaksi->total = $total;
        //     $transaksi->save();
        // }

        // \Cart::session($userId)->clear();
        // alert()->success('Sukses', 'Pesanan Berhasil Ditambahkan')->autoclose(5000);
        // return redirect()->route('detail.transaksi',$transaksi->id);

        return view('konsumen.checkout',$data);
    }

    public function prosescheckout(Request $request)
    {
        $jamSekarang = Carbon::now()->timestamp;
        $jam7 = Carbon::parse('07:00:00')->timestamp;
        $jam14 = Carbon::parse('14:00:00')->timestamp;
        if($jamSekarang >= $jam7 && $jamSekarang <= $jam14){
            $userId = auth()->user()->id;
            $brg = \Cart::session($userId)->getContent();
            // dd($brg);
            $transaksi = new Pembelian();
            $transaksi->id_konsumen = Auth::user()->id;
            $transaksi->keterangan_pembelian = $request->keterangan_pembelian;
            $transaksi->total_tr = \Cart::getTotal();
            $transaksi->save();
            foreach($brg as $i)
            {
                $dt_produk = Barang::find($i->id);
                $detTr = new DetailPembelian();
                $detTr->pembelian_id = $transaksi->id;
                $detTr->id_barang = $i->id;
                $detTr->harga =  $i->price;
                $detTr->qty = $i->quantity;
                $detTr->save(); 
    
                $dt_produk->barang_stok = ($dt_produk->barang_stok - $i->quantity);
                $dt_produk->save();
            }
            $bayar = new Pembayaran();
            $bayar->id_pembelian = $transaksi->id;
            $bayar->bank_id = $request->bank_id;            
            $bayar->user_id = Auth::user()->id;
            $bayar->save();
    
            $transfer = new BuktiTransfer();
            $transfer->id_pembelian = $transaksi->id;
            $transfer->is_verified = 0;
            $transfer->save();
    
            $kirim = new Pengiriman();
            $kirim->id_pembelian = $transaksi->id;
            $kirim->status_dikirim = 0;
            $kirim->status_sampai = 0;
            $kirim->save();
    
            \Cart::session($userId)->clear();
            alert()->success('Sukses', 'Pesanan Berhasil Ditambahkan')->autoclose(5000);
            return redirect('detail-tr/'.$transaksi->id);
        }else{
            alert()->error('Gagal', 'Checkout hanya bisa dilakukan jam 8 pagi sampai 2 siang')->autoclose(5000);
            return redirect()->back();
        }
    }
}
