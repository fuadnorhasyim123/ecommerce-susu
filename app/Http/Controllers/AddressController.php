<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AddressController extends Controller
{

    public function getProvinsi(Request $request)
    {
        $search = $request->search;
        if ($search != "") {
            $provinsi = DB::table('provinces')->where('name', 'like', '%' . $search . '%')
                ->select('city_id as id', 'name as text')
                ->get();
        } else {
            $provinsi = DB::table('provinces')->select('province_id as id', 'name as text')
                ->get();
        }
        return response()->json($provinsi);
    }
    
    public function getCities(Request $request)
    {
        $search = $request->search;
        $provinsi = $request->provinsi;
        if ($search != "") {
            $cities = DB::table('cities')->where('province_id', $provinsi)
                ->where('name', 'like', '%' . $search . '%')
                ->select('city_id as id', 'name as text')
                ->get();
        } else {
            $cities = DB::table('cities')->where('province_id', $provinsi)
                ->select('city_id as id', 'name as text')
                ->get();
        }
        return response()->json($cities);
    }

}
