<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tcust = DB::table('users')->where('role', 'customer')->count();
        $tbrg = DB::table('barang')->count();
        $ttrx = DB::table('pembelian')->count();
        $ttrxsls = DB::table('pembelian')->where('status', '1')->count();
        //chart
        $trx_produk = DB::table('detail_pembelian')
            ->select('detail_pembelian.id_barang', 'barang.barang_nama', DB::raw('sum(detail_pembelian.qty) as total'))
            ->join('barang', 'barang.barang_id', '=', 'detail_pembelian.id_barang')
            ->groupBy('detail_pembelian.id_barang')
            ->get();

        $trx_user = DB::table('pembelian')
            ->select('pembelian.id_konsumen', 'cities.name as provinsi', DB::raw('sum(pembelian.total_tr) as total'))
            ->join('users', 'users.id', '=', 'pembelian.id_konsumen')
            ->join('cities', 'cities.city_id', '=', 'users.kota_id')
            ->join('provinces', 'provinces.province_id', '=', 'users.provinsi_id')
            ->groupBy('pembelian.id_konsumen')
            ->get();

        return view('admin.dashboard.dashboard', [
            'trx_produk'    => $trx_produk,
            'trx_user'    => $trx_user,
            'tcust'    => $tcust,
            'tbrg'    => $tbrg,
            'ttrx'    => $ttrx,
            'ttrxsls'    => $ttrxsls,
        ]);
    }
}
