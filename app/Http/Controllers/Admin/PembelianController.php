<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;

class PembelianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Barang.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $pembelian = DB::table('pembelian')
                ->select('pembelian.id', 'pembelian.total_tr', 'pembelian.created_at', 'pembelian.status', 'users.name', 'bukti_transfer.is_verified', 'bukti_transfer.bukti_transfer')
                ->join('users', 'pembelian.id_konsumen', '=', 'users.id')
                ->join('bukti_transfer', 'pembelian.id', '=', 'bukti_transfer.id_pembelian')
                ->orderByDesc('created_at')->get();

            return DataTables::of($pembelian)
                ->addColumn('aksi', function ($row) {
                    $data = '<a href="javascript:void(0)" class="btn btn-danger btn-icon-text" id="btnHapus" data-toggle="modal" data-id="' . $row->id . '"><i class="mdi mdi-trash-can-outline"></i></a>
                            <meta name="csrf-token" content="{{ csrf_token() }}">';
                    return $data;
                })
                ->addColumn('status_bayar', function ($row) {
                    if ($row->bukti_transfer == "") {
                        $data = 'Menunggu Upload Bukti';
                    } elseif ($row->bukti_transfer != "" && $row->is_verified == "0") {
                        $data = '<a href="javascript:void(0)" class="btn btn-success btn-icon-text" id="btnVerif" data-toggle="modal" data-id="' . $row->id . '"> Verifikasi</a>
                                <meta name="csrf-token" content="{{ csrf_token() }}"';
                    } elseif ($row->is_verified == "1") {
                        $data = 'Pembayaran Sudah Diverifikasi';
                    } elseif ($row->is_verified == "2") {
                        $data = 'Pembayaran Ditolak, Menunggu Upload Ulang';
                    }
                    return $data;
                })
                ->rawColumns(['aksi', 'status_bayar'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.transaksi.pembelian');
    }

    public function edit($id)
    {
        $pembelian = DB::table('pembelian')->select('pembelian.*', 'bukti_transfer.bukti_transfer')->join('bukti_transfer', 'pembelian.id', '=', 'bukti_transfer.id_pembelian')->where('pembelian.id', $id)->first();

        return Response::json($pembelian);
    }

    public function destroy(Request $request)
    {
        DB::table('pembelian')->where('id', $request->id1)->delete();
        DB::table('pembayaran')->where('id_pembelian', $request->id1)->delete();
        DB::table('bukti_transfer')->where('id_pembelian', $request->id1)->delete();
        DB::table('detail_pembelian')->where('pembelian_id', $request->id1)->delete();
        DB::table('pengiriman')->where('id_pembelian', $request->id1)->delete();

        Alert::success('Sukses', 'Pembelian Berhasil Dihapus');

        return redirect("/admin/pembelian");
    }
    public function verif(Request $request)
    {

        DB::table('bukti_transfer')->where('id_pembelian', $request->id2)->update([
            'is_verified'   => $request->is_verified,
            'keterangan'   => $request->ket
        ]);

        Alert::success('Sukses', 'Pembelian Berhasil Dihapus');

        return redirect("/admin/pembelian");
    }
}
