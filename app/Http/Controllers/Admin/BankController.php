<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;

class BankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Barang.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $bank = DB::table('bank')->orderBy('nama_bank')->get();

            return DataTables::of($bank)
                ->addColumn('aksi', function ($row) {
                    $data = '<a href="<a href="javascript:void(0)" class="btn btn-warning btn-icon-text" id="btnEdit" data-toggle="modal" data-id="' . $row->id . '"><i class="mdi mdi-pencil-box"></i></a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger btn-icon-text" id="btnHapus" data-toggle="modal" data-id="' . $row->id . '"><i class="mdi mdi-trash-can-outline"></i></a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">';
                    return $data;
                })
                ->rawColumns(['aksi'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.master.bank');
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {

            DB::table('bank')->insert([
                'nama_bank'     => $request->nama_bank,
                'no_rekening'     => $request->no_rekening,
            ]);

            Alert::success('Sukses', 'Bank Berhasil Ditambah');
            return redirect("/admin/bank");
        } else if ($request->action == 'edit') {

            DB::table('bank')->where('id', $request->id)->update([
                'nama_bank'     => $request->nama_bank,
                'no_rekening'     => $request->no_rekening,
            ]);

            Alert::success('Sukses', 'Bank Berhasil Diedit');
            return redirect("/admin/bank");
        }
    }

    public function edit($id)
    {
        $bank = DB::table('bank')->where('id', $id)->first();

        return Response::json($bank);
    }

    public function destroy(Request $request)
    {
        DB::table('bank')->where('id', $request->id1)->delete();

        Alert::success('Sukses', 'Bank Berhasil Dihapus');

        return redirect("/admin/bank");
    }
}
