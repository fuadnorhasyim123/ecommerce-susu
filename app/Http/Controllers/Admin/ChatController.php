<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Models\Chat;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Barang.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $chats = DB::table('chat')
                ->select('users.id', 'users.username', 'users.alamat', 'users.telepon')
                ->join('users', 'users.id', '=', 'chat.sender_id')
                ->where('role', '!=', 'admin')
                ->groupBy('chat.sender_id')
                ->orderByDesc('chat.created_at')
                ->get();

            return DataTables::of($chats)
                ->addColumn('aksi', function ($row) {
                    $data = '<a href="<a href="javascript:void(0)" class="btn btn-warning btn-icon-text" id="btnDetail" data-toggle="modal" data-id="' . $row->id . '"><i class="mdi mdi-eye"></i></a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">';
                    return $data;
                })
                ->rawColumns(['aksi'])
                ->addIndexColumn()
                ->make(true);
        }

        $customer = DB::table('users')->where('role', 'customer')->get();

        return view('admin.notifikasi.chat', [
            'customer' => $customer
        ]);
    }

    public function store(Request $request)
    {
        Chat::create([
            'sender_id' => \Auth::user()->id,
            'receiver_id' => $request->sender_id,
            'message' => $request->message,
            'is_read_by_sender' => 1,
            'is_read_by_receiver' => 0
        ]);

        Alert::success('Sukses', 'Pesan Berhasil Dikirim');
        return redirect("/admin/chat");
    }

    public function edit($id)
    {
        $chat = DB::table('chat')
            ->select('chat.*', 'users.username', 'users.alamat', 'users.telepon')
            ->join('users', 'users.id', '=', 'chat.sender_id')
            ->where('chat.id', $id)->first();

        return Response::json($chat);
    }

    public function loadMessage($id)
    {
        Chat::where('receiver_id', \Auth::user()->id)
            ->update([
                'is_read_by_receiver' => 1
            ]);
        $chats = Chat::join('users as sender', 'sender.id', '=', 'chat.sender_id')
            ->join('users as receiver', 'receiver.id', '=', 'chat.receiver_id')
            ->where('chat.sender_id', \Auth::user()->id)
            ->where('chat.receiver_id', $id)
            ->orWhere('chat.sender_id', $id)
            ->where('chat.receiver_id', \Auth::user()->id)
            ->select(
                'chat.id',
                'chat.message as pesan',
                'chat.sender_id',
                'chat.receiver_id',
                'sender.name as sender',
                'receiver.name as receiver',
                'chat.created_at'
            )
            ->get();

        return response()->json(["code" => 200, "data" => $chats]);
    }

    public function sendMessage(Request $request)
    {
        Chat::create([
            'sender_id' => \Auth::user()->id,
            'receiver_id' => $request->id,
            'message' => $request->message,
            'is_read_by_sender' => 0,
            'is_read_by_receiver' => 1
        ]);
        return response()->json(["code" => 200, "message" => "Berhasil kirim pesan"]);
    }

    public function cekUnReadMessage()
    {
        $count = Chat::where('receiver_id', \Auth::user()->id)
            ->where('is_read_by_receiver', 0)->count();
        return response()->json(["code" => 200, "data" => $count]);
    }
}
