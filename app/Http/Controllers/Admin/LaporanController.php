<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->get('tgl_awal') || $request->get('tgl_akhir')) {
                $pembelian = DB::table('pembelian')
                    ->select('pembelian.*', 'a.name', 'a.email', 'a.alamat', 'b.nama_bank')
                    ->join('users as a', 'pembelian.id_konsumen', '=', 'a.id')
                    ->join('pembayaran', 'pembayaran.id_pembelian', '=', 'pembelian.id')
                    ->join('bank as b', 'pembayaran.bank_id', '=', 'b.id')
                    ->whereBetween('pembelian.created_at', [$request->get('tgl_awal'), $request->get('tgl_akhir')])
                    ->get();
            } else {
                $pembelian = DB::table('pembelian')
                    ->select('pembelian.*', 'a.name', 'a.email', 'a.alamat', 'b.nama_bank')
                    ->join('users as a', 'pembelian.id_konsumen', '=', 'a.id')
                    ->join('pembayaran', 'pembayaran.id_pembelian', '=', 'pembelian.id')
                    ->join('bank as b', 'pembayaran.bank_id', '=', 'b.id')
                    ->get();
            }

            return DataTables::of($pembelian)
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.laporan.laporan-pembelian');
    }
}
