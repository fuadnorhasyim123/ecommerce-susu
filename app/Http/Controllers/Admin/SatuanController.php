<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;

class SatuanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Barang.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $satuan = DB::table('satuan')->orderBy('satuan_nama')->get();

            return DataTables::of($satuan)
                ->addColumn('aksi', function ($row) {
                    $data = '<a href="<a href="javascript:void(0)" class="btn btn-warning btn-icon-text" id="btnEdit" data-toggle="modal" data-id="' . $row->satuan_id . '"><i class="mdi mdi-pencil-box"></i></a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger btn-icon-text" id="btnHapus" data-toggle="modal" data-id="' . $row->satuan_id . '"><i class="mdi mdi-trash-can-outline"></i></a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">';
                    return $data;
                })
                ->rawColumns(['aksi'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.master.satuan');
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {

            DB::table('satuan')->insert([
                'satuan_nama'     => $request->satuan_nama,
            ]);

            Alert::success('Sukses', 'Satuan Berhasil Ditambah');
            return redirect("/admin/satuan");
        } else if ($request->action == 'edit') {

            DB::table('satuan')->where('satuan_id', $request->satuan_id)->update([
                'satuan_nama'     => $request->satuan_nama,
            ]);

            Alert::success('Sukses', 'Satuan Berhasil Diedit');
            return redirect("/admin/satuan");
        }
    }

    public function edit($id)
    {
        $satuan = DB::table('satuan')->where('satuan_id', $id)->first();

        return Response::json($satuan);
    }

    public function destroy(Request $request)
    {
        DB::table('satuan')->where('satuan_id', $request->satuan_id1)->delete();

        Alert::success('Sukses', 'Satuan Berhasil Dihapus');

        return redirect("/admin/satuan");
    }
}
