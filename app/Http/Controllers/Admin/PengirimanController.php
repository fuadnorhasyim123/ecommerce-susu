<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;

class PengirimanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Barang.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $pengiriman = DB::table('pengiriman')
                ->select('pengiriman.*', 'pembelian.created_at')
                ->join('pembelian', 'pembelian.id', '=', 'pengiriman.id_pembelian')
                ->orderByDesc('pembelian.created_at')->get();

            return DataTables::of($pengiriman)
                ->addColumn('sk', function ($row) {
                    if ($row->status_dikirim == "0") {
                        $data = '<a href="javascript:void(0)" class="btn btn-success btn-icon-text" id="btnKirim" data-toggle="modal" data-id="' . $row->id . '"> Kirim</a>
                                <meta name="csrf-token" content="{{ csrf_token() }}"';
                    } elseif ($row->status_dikirim == "1") {
                        $data = 'Sudah Dikirim';
                    }
                    return $data;
                })
                ->addColumn('ss', function ($row) {
                    if ($row->status_dikirim != "0") {
                        if ($row->status_sampai == "0") {
                            $data = 'Menunggu Konfirmasi Customer';
                        } elseif ($row->status_dikirim == "1") {
                            $data = 'Sudah Diterima Customer';
                        }
                    } else {
                        $data = 'Menunggu Konfirmasi Admin';
                    }

                    return $data;
                })
                ->rawColumns(['sk', 'ss'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.transaksi.pengiriman');
    }

    public function edit($id)
    {
        $pengiriman = DB::table('pengiriman')->where('id', $id)->first();

        return Response::json($pengiriman);
    }

    public function kirim(Request $request)
    {
        DB::table('pengiriman')->where('id', $request->id2)->update([
            'status_dikirim'   => '1',
            'tanggal_pengiriman'    => \Carbon\carbon::now()
        ]);

        Alert::success('Sukses', 'Pembelian Berhasil Dikirim');

        return redirect("/admin/pengiriman");
    }
}
