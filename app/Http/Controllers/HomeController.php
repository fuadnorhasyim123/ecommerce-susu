<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user();
        if ($userId !== null) {
            $cart = \Cart::session(Auth::user()->id)->getContent();
        } else {
            $cart = \Cart::getContent();
        }
        $data = [
            'produk' => DB::table('barang')->get(),
            'cart' => $cart
        ];
        return view('home', ['data' => $data]);
    }

    public function auth()
    {
        return view('konsumen.auth');
    }
}
